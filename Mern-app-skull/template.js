//bundled frontend code in the HTML to render our React frontend,
// we will update the template.js 
//file so that it adds the script file from the dist folder
// to the end of the <body> tag
// yarn add react react-dom
// yarn add react-router react-router-dom
// modules are for declarative routing and bookmarkable URL routes in frontend
// yarn add @material-ui/core @material-ui/icons
// Roboto fonts in the head and Material-UI icons 

export default ({markup, css}) => {
    return `<!doctype html>
      <html lang="en">
        <head>
          <meta charset="utf-8">
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
          >
          <title>MERN Skeleton</title>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400">
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
          <style>
              a{
                text-decoration: none;
                color: #061d95
              }
          </style>
        </head>
        <body style="margin:0">
          <div id="root">${markup}</div>
          <style id="jss-server-side">${css}</style>
          <script type="text/javascript" src="/dist/bundle.js"></script>
        </body>
      </html>`
}

