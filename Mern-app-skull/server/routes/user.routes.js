//Protecting user routes
import express from 'express'
import userCtrl from '../controllers/user.controller'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()

router.route('/api/users')
//Listing users with GET
  .get(userCtrl.list)
//Creating a new user with POST, calls the create function defined in controller
  .post(userCtrl.create)

//Fetching a user with GET
//Updating a user with PUT
//Deleting a user with DELETE
//read, update and delete routes
//two auth controller methods called requireSignin and hasAuthorization
//authentication and authorization
router.route('/api/users/:userId')
  .get(authCtrl.requireSignin, userCtrl.read)
  .put(authCtrl.requireSignin, authCtrl.hasAuthorization, userCtrl.update)
  .delete(authCtrl.requireSignin, authCtrl.hasAuthorization, userCtrl.remove)

//route that matches a path containing the :userId parameter in it, 
//the app will execute the userByID controller function
router.param('userId', userCtrl.userByID)

export default router
