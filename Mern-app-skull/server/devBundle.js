//During development, run the server, 
//the Express app should also load the Webpack middleware 
//that's relevant to the frontend with 
//respect to the configuration that's been set for the client-side code,
//so that the frontend and backend development workflow is integrated
//Webpack middleware uses the values set in webpack.config.
//client.js, and we enable hot reloading
// from the server-side using Webpack Hot Middleware
//import and call this compile method in express.js 
//by adding the following highlighted lines, but only during development
//add to express.js by import './devBundle'
// line 22 N 29 comment out for production in express.js
//bundle code will be placed in the dist folder 
//the front end views that we will see rendered in the browser will load from
//the bundle files in the dist folder
//express app serves static files that arent generated dynamically by server side code
import config from './../config/config'
import webpack from 'webpack'
import webpackMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackConfig from './../webpack.config.client.js'

const compile = (app) => {
  if(config.env === "development"){
    const compiler = webpack(webpackConfig)
    const middleware = webpackMiddleware(compiler, {
      publicPath: webpackConfig.output.publicPath
    })
    app.use(middleware)
    app.use(webpackHotMiddleware(compiler))
  }
}

export default {
  compile
}
